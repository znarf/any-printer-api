<?php

define('root_dir', __DIR__);

define('app_dir', root_dir);

define('storage_dir', root_dir . '/storage');

define('spool_dir', root_dir . '/spool');

require root_dir . '/vendor/amateur/amateur.dsl.php';

require_once amateur_dir . '/core/replaceable.functions.php';

start( app_dir );
