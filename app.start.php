<?php

function json($value)
{
  set_header('Content-Type', 'application/json');
  echo json_encode($value, JSON_PRETTY_PRINT);
  exit;
}

function json_forbidden($message = 'Forbidden')
{
  status('403');
  json(['message' => $message]);
}

function json_not_found($message = 'Not Found')
{
  status('404');
  json(['message' => $message]);
}

function json_error($message = 'Invalid Request')
{
  status('400');
  json(['message' => $message]);
}

function get_storage($name)
{
  return json_decode(file_get_contents(storage_dir . '/' . $name . '.json'), true);
}

function get_printer_jobs($printer_id)
{
  $printer = get_target_printer($printer_id);
  $dirname = spool_dir . '/' . $printer_id;
  $jobs = [];
  if (file_exists($dirname) && $handle = opendir($dirname)) {
    while ($entry = readdir($handle)) {
      if ($entry != "." && $entry != "..") {
        $jobs[$entry] = ['printer' => $printer] + json_decode(file_get_contents($dirname . '/' . $entry), true);
      }
    }
    closedir($handle);
  }
  return $jobs;
}

function get_broker_jobs($broker_id)
{
  $jobs = [];
  $broker = get_target_broker($broker_id);
  foreach ($broker['printers'] as $printer_id) {
    $jobs += get_printer_jobs($printer_id);
  }
  return $jobs;
}

function register_job($printer_id, $photo_url)
{
  if (!$file = file_get_contents($photo_url)) {
    json_error('Invalid parameter (photo_url)');
  }
  $filename = spool_dir . '/' . $printer_id . '/' . md5($photo_url);
  if (!file_exists(dirname($filename))) {
    mkdir(dirname($filename), 0777, true);
  }
  file_put_contents($filename, json_encode(['photo_url' => $photo_url]));
  chmod($filename, 0777);
}

function delete_job($broker_id, $job_id)
{
  foreach (get_broker_jobs($broker_id) as $id => $job) {
    if ($id == $job_id) {
      unlink(spool_dir . '/' . $job['printer']['id'] . '/' . $id);
      return true;
    }
  }
}

function get_target_broker($id)
{
  $brokers = get_storage('brokers');
  return isset($brokers[$id]) ? $brokers[$id] : json_not_found('Broker Not Found');
}

function get_target_printer($id)
{
  $printers = get_storage('printers');
  return isset($printers[$id]) ? $printers[$id] : json_not_found('Printer Not Found');
}

function get_app($id)
{
  $apps = get_storage('apps');
  return isset($apps[$id]) ? $apps[$id] : json_not_found('App Not Found');
}

if (url_is('/')) {
  json(['message' => 'Welcome to Any Printer API']);
}

elseif (url_is('/apps')) {
  # Should be restricted :-)
  $apps = get_storage('apps');
  json($apps);
}
elseif (url_is('/printers')) {
  # Should be restricted :-)
  $printers = get_storage('printers');
  json($printers);
}
elseif (url_is('/brokers')) {
  # Should be restricted :-)
  $brokers = get_storage('brokers');
  json($brokers);
}

# Browse Jobs
elseif ($matches = url_match('/brokers/*/jobs')) {
  $broker_id = $matches[1];
  $broker = get_target_broker($broker_id);
  # Check Broker Credentials
  if ($broker['client_secret'] != get_param('client_secret')) {
    json_forbidden();
  }
  # Out Jobs
  return json(get_broker_jobs($broker_id));
}

# New Job
elseif ($matches = url_match('/printers/*/jobs')) {
  $printer_id = $matches[1];
  $broker = get_target_printer($printer_id);
  # Get App and Check Credentials
  $app = get_app(get_param('client_id'));
  if ($app['client_secret'] != get_param('client_secret')) {
    json_forbidden();
  }
  # Register Job
  if (is_post() && has_param('photo_url')) {
    register_job($printer_id, get_param('photo_url'));
    return json(['message' => 'Success!']);
  }
  return json(['message' => 'Failure :-(']);
}

# Job Done
elseif ($matches = url_match('/brokers/*/jobs/*')) {
  $broker_id = $matches[1];
  $job_id = $matches[2];
  $broker = get_target_broker($broker_id);
  # Check Broker Credentials
  if ($broker['client_secret'] != get_param('client_secret')) {
    json_forbidden();
  }
  # Delete Job
  if (is_delete()) {
    delete_job($broker_id, $job_id);
    return json(['message' => 'Success!']);
  }
  return json(['message' => 'Failure :-(']);
}
